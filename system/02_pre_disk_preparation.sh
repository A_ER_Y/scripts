#!/usr/bin/env bash
# 02_pre_disk_preparation.sh

lsblk -o name,fstype,size,type,mountpoint
read -p "select disk: " DISK
if [[ ! $DISK =~ sd[a-z] ]]; then
    PARTITIONS=( "p1" "p2" )
else
    PARTITIONS=( "1" "2" )
fi

echo "partition disk"
wipefs --all --force /dev/$DISK
parted /dev/$DISK mklabel gpt
parted -a optimal /dev/$DISK mkpart primary fat32 0% 1GiB
parted -a optimal /dev/$DISK mkpart primary btrfs 1GiB 100%
parted /dev/$DISK set 1 esp on

echo "encrypt root partition"
cryptsetup luksFormat --type luks2 /dev/${DISK}${PARTITIONS[1]}
cryptsetup luksOpen /dev/${DISK}${PARTITIONS[1]} cryptroot

echo "make filesystems"
mkfs.fat -F32 /dev/${DISK}${PARTITIONS[0]}
mkfs.btrfs -fL cryptroot /dev/mapper/cryptroot

echo "mount cryptroot"
mount /dev/mapper/cryptroot /mnt
mkdir -p /mnt/boot/efi /mnt/home
