#!/usr/bin/env bash
# 07_tpm2_unlocking.sh

lsblk -o name,fstype,size,type,mountpoint
read -p "select luks partition: " LUKS_PARTITION

echo "generate recovery key"
sudo systemd-cryptenroll /dev/$LUKS_PARTITION --recovery-key > tpm2_unlocking_recovery_key.txt

echo "enroll key into tpm"
sudo systemd-cryptenroll /dev/$LUKS_PARTITION --tpm2-device=auto --tpm2-pcrs=0,7
