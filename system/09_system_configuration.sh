#!/usr/bin/env bash
# 09_system_configuration.sh

echo "install packages"
sudo pacman -S adobe-source-code-pro-fonts alacritty bleachbit bottom brightnessctl cpupower docker docker-buildx docker-compose eza fzf gammastep git go-task httpie mpv neofetch newsboat nodejs npm pass pavucontrol perl-rename pnpm python-adblock python-mutagen qrtool qutebrowser reflector sane-airscan slurp swappy swaybg swayimg tesseract-data-eng timeshift ttf-dejavu ttf-joypixels unzip wf-recorder wl-clipboard xdg-desktop-portal-hyprland ydotool yt-dlp zathura zathura-pdf-mupdf zip

echo "install aur packages"
cd /opt && sudo git clone https://aur.archlinux.org/paru-bin.git && sudo chown -R eray:eray paru-bin && cd paru-bin && makepkg -si
cd
paru -S --skipreview brother-mfc-l2700dw freetube-bin grimblast-git hyprland-nox jmtpfs nvim-packer-git polychromatic python-httpie-jwt-auth swaylock-effects-git tofi

echo "configure bleachbit"
sudo chmod 755 /usr/share/secureboot

echo "configure cpupower"
sudo echo "eray ALL=(ALL) NOPASSWD: /usr/bin/cpupower frequency-set --governor *" >> /etc/sudoers

echo "configure docker"
sudo gpasswd -a eray docker
sudo systemctl enable docker.service

echo "configure go-task"
sudo ln -s /usr/bin/go-task /usr/bin/task

echo "configure polychromatic"
sudo gpasswd -a eray plugdev

echo "configure ydotool"
sudo gpasswd -a eray input
systemctl --user enable ydotool

echo "link configuration files"
rm .bash_profile .bashrc
mkdir -p .config/alacritty .config/FreeTube .config/mpv .config/.newsboat .config/openrazer .config/qutebrowser/bookmarks .config/swappy .config/swayimg .config/tofi .config/zathura .ssh/inuits
[[ ! -e ".ssh/config" ]] && ln -sf /home/eray/documents/.credentials/ssh/config .ssh/config
[[ ! -e ".ssh/inuits/config" ]] && ln -sf /home/eray/documents/.credentials/ssh/inuits/config .ssh/inuits/config
[[ ! -e ".ssh/inuits/id_rsa" ]] && ln -sf /home/eray/documents/.credentials/ssh/inuits/id_rsa .ssh/inuits/id_rsa
[[ ! -e ".ssh/inuits/id_rsa.pub" ]] && ln -sf /home/eray/documents/.credentials/ssh/inuits/id_rsa.pub .ssh/inuits/id_rsa.pub
[[ ! -e ".ssh/inuits/inuits-ssh" ]] && ln -sf /home/eray/documents/.credentials/ssh/inuits/inuits-ssh .ssh/inuits/inuits-ssh
[[ ! -e ".bash_profile" ]] && ln -sf /home/eray/informatics/os/linux/configuration_files/bash/.bash_profile .bash_profile
[[ ! -e ".bashrc" ]] && ln -sf /home/eray/informatics/os/linux/configuration_files/bash/.bashrc .bashrc
[[ ! -e ".gitconfig" ]] && ln -sf /home/eray/informatics/os/linux/configuration_files/git/.gitconfig .gitconfig
[[ ! -e ".config/hypr" ]] && ln -sf /home/eray/informatics/os/linux/configuration_files/hyprland .config/hypr
[[ ! -e ".config/nvim" ]] && ln -sf /home/eray/informatics/os/linux/configuration_files/nvim .config/nvim
[[ ! -e ".config/openrazer/razer.conf" ]] && ln -sf /home/eray/informatics/os/linux/configuration_files/razer/razer.conf .config/openrazer/razer.conf
[[ ! -e ".config/qutebrowser/bookmarks/urls" ]] && ln -sf /home/eray/informatics/os/linux/configuration_files/qutebrowser/urls .config/qutebrowser/bookmarks/urls
[[ ! -e ".config/qutebrowser/config.py" ]] && ln -sf /home/eray/informatics/os/linux/configuration_files/qutebrowser/config.py .config/qutebrowser/config.py
[[ ! -e ".config/qutebrowser/quickmarks" ]] && ln -sf /home/eray/informatics/os/linux/configuration_files/qutebrowser/quickmarks .config/qutebrowser/quickmarks
[[ ! -e ".config/swappy/config" ]] && ln -sf /home/eray/informatics/os/linux/configuration_files/swappy.conf .config/swappy/config
[[ ! -e ".config/wireplumber" ]] && ln -sf /home/eray/informatics/os/linux/configuration_files/wireplumber .config/wireplumber

echo "configure nvim"
sudo pacman -S vue-typescript-plugin
nvim --headless -c "PackerSync" -c "TSUpdateSync" -c "MasonUpdate" -c "autocmd User MasonUpdateAllComplete quitall" -c "MasonUpdateAll"

echo "manual configuration"
echo "bleachbit"
echo "brother-mfc-l2700dw - http://localhost:631/"
echo "pavucontrol"
echo "timeshift-gtk"

echo "reboot with sudo"
