#!/usr/bin/env bash
# 01_pre_setup.sh

echo "set keyboard layout"
loadkeys be-latin1

echo "connect to internet"
systemctl start dhcpcd.service

echo "update system clock"
timedatectl set-ntp true
timedatectl set-timezone Europe/Brussels
