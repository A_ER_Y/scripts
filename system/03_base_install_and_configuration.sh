#!/usr/bin/env bash
# 03_base_install_and_configuration.sh

echo "select mirrors"
reflector --country Belgium,Netherlands,Germany --latest 3 --sort age --save /etc/pacman.d/mirrorlist

echo "install base packages"
pacstrap /mnt android-udev archiso avahi base base-devel bash-completion bluez bluez-utils btrfs-progs cmake cronie curl cryptsetup cups dialog dkms dosfstools exfat-utils ghostscript gnupg intel-media-driver intel-ucode linux linux-firmware linux-headers man-db man-pages mkinitcpio mtools mtpfs neovim networkmanager nss-mdns ntfs-3g openssh pacman-contrib parted pipewire pipewire-audio pipewire-pulse pv rsync sbctl sudo texinfo util-linux vulkan-icd-loader vulkan-intel wget wireless_tools wpa_supplicant

cat <<EOF > /mnt/root/base_configuration.sh
echo "configure host"
echo "AERY" > /etc/hostname
echo -e "\n127.0.0.1\tlocalhost" >> /etc/hosts
echo -e "::1\t\tlocalhost" >> /etc/hosts
echo -e "127.0.1.1\tAERY.localdomain\tAERY" >> /etc/hosts

echo "configure timezone"
ln -sf /usr/share/zoneinfo/Europe/Brussels /etc/localtime
hwclock --systohc
timedatectl set-ntp true

echo "configure locale"
sed -i "/^#en_US.UTF-8 UTF-8/s/#//" /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "KEYMAP=be-latin1" > /etc/vconsole.conf

echo "configure pacman"
sed -i "/^#Color$/s/#//" /etc/pacman.conf
sed -i "/^#VerbosePkgLists$/s/#//" /etc/pacman.conf
sed -i "s/^#ParallelDownloads = 5$/ParallelDownloads = 8/" /etc/pacman.conf

echo "configure nss-mdns"
sed -i "s/^hosts: mymachines.*files/\0 mdns4_minimal [NOTFOUND=return]/" /etc/nsswitch.conf
sed -i "s/^hosts: mymachines.*dns/\0 mdns4/" /etc/nsswitch.conf

echo "configure faillock"
sed -i "s/^# deny = 3$/  deny = 0/" /etc/security/faillock.conf

echo "configure lid switch"
sed -i "s/^#HandleLidSwitch=suspend$/HandleLidSwitch=ignore/" /etc/systemd/logind.conf
sed -i "s/^#HandleLidSwitchExternalPower=suspend$/HandleLidSwitchExternalPower=ignore/" /etc/systemd/logind.conf

echo "configure user"
useradd -m -d /home/eray -s /bin/bash -G wheel,video eray
passwd eray
sed -i "/^# %wheel ALL=(ALL:ALL) ALL$/s/# //" /etc/sudoers
echo -e "\nDefaults:eray timestamp_timeout=20" >> /etc/sudoers
echo "eray ALL=(ALL) NOPASSWD: /usr/bin/cpupower frequency-set --governor powersave" >> /etc/sudoers
echo "eray ALL=(ALL) NOPASSWD: /usr/bin/cpupower frequency-set --governor performance" >> /etc/sudoers

echo "enable/mask services"
systemctl enable avahi-daemon.service
systemctl enable bluetooth.service
systemctl enable cups.service
systemctl enable NetworkManager
systemctl mask systemd-random-seed

exit
EOF

chmod +x /mnt/root/base_configuration.sh
arch-chroot /mnt /root/base_configuration.sh
rm /mnt/root/base_configuration.sh
