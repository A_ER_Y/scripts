#!/usr/bin/env bash
# timeshift_restore.sh

if [[ "$1" != "local" && "$1" != "live" ]]; then
    echo "Invalid argument. Supported arguments: local, live."
    exit
fi

timeshift --list
clear
timeshift --btrfs --list
read -p "enter snapshot name: " SNAPSHOT_NAME

timeshift --btrfs --restore --snapshot "$SNAPSHOT_NAME"
if [[ "$?" == "0" ]]; then
    mount /dev/dm-0 /mnt
    if [[ "$1" == "local" ]]; then
        rsync -aHAXvh --force --delete /mnt/timeshift-btrfs/snapshots/$SNAPSHOT_NAME/@/.boot/* /boot
    elif [[ "$1" == "live" ]]; then
        lsblk -o name,fstype,size,type,mountpoint
        read -p "select boot partition: " BOOT_PARTITION
        mount /dev/$BOOT_PARTITION /mnt/@/boot/efi
        rsync -aHAXvh --force --delete /mnt/timeshift-btrfs/snapshots/$SNAPSHOT_NAME/@/.boot/* /mnt/@/boot
        umount /mnt/@/boot/efi
    fi
    umount /mnt
fi
