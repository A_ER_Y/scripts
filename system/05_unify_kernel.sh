#!/usr/bin/env bash
# 05_unify_kernel.sh

echo "prepare kernel unifying"
mkdir -p /mnt/@/boot/efi/EFI/Linux

echo "configure mkinitcpio"
sed -i "/^HOOKS=/s/base udev/systemd/" /mnt/@/etc/mkinitcpio.conf
sed -i "/^HOOKS=/s/keymap consolefont/sd-vconsole/" /mnt/@/etc/mkinitcpio.conf
sed -i "/^HOOKS=/s/block/block sd-encrypt/" /mnt/@/etc/mkinitcpio.conf
sed -i "/^HOOKS=/s/ fsck//" /mnt/@/etc/mkinitcpio.conf
sed -i "/^#ALL_config/s/#//" /mnt/@/etc/mkinitcpio.d/linux.preset
sed -i "/^default_image/s/d/#d/" /mnt/@/etc/mkinitcpio.d/linux.preset
sed -i "/^#default_uki/s/#//" /mnt/@/etc/mkinitcpio.d/linux.preset
sed -i "/^fallback_image/s/f/#f/" /mnt/@/etc/mkinitcpio.d/linux.preset
sed -i "/^#fallback_uki/s/#//" /mnt/@/etc/mkinitcpio.d/linux.preset
sed -i 's_="/efi_="/boot/efi_g' /mnt/@/etc/mkinitcpio.d/linux.preset

echo "configure boot loader"
umount -l /mnt
mount -o subvol=@,defaults,rw /dev/mapper/cryptroot /mnt
arch-chroot /mnt mount -a
UUID=$(lsblk -f | grep crypto_LUKS | tr -s " " | cut -d" " -f4)
arch-chroot /mnt bootctl install --esp-path=/boot/efi
echo "rd.luks.name=${UUID}=cryptroot rd.luks.options=tpm2-device=auto root=/dev/mapper/cryptroot rootflags=subvol=@ i915.fastboot=1 mitigations=off fsck.mode=skip nowatchdog bgrt_disable systemd.show_status=true" > /mnt/etc/kernel/cmdline
echo -e "default arch-linux.efi\ntimeout 0" > /mnt/boot/efi/loader/loader.conf
echo -e "blacklist iTCO_wdt\nblacklist pcspkr\nblacklist joydev\nblacklist mousedev\nblacklist mac_hid" > /etc/modprobe.d/blacklists.conf

echo "generate uki"
arch-chroot /mnt mkinitcpio -P
cp /root/*.sh /mnt/home/eray
echo "reboot with 'systemctl reboot --firmware-setup' and set secure boot to setup mode after clearing all keys"
