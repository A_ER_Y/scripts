#!/usr/bin/env bash
# 08_sync_drive_with_backup_drive.sh

lsblk -o name,fstype,size,type,mountpoint
read -p "enter backup drive name: " backup_drive_name

echo "mount backup drive"
sudo cryptsetup luksOpen /dev/$backup_drive_name ${backup_drive_name}_luks
backup_drive_name="${backup_drive_name}_luks"
sudo mkdir /mnt/$backup_drive_name
sudo mount /dev/mapper/$backup_drive_name /mnt/$backup_drive_name

echo "sync drive with backup drive"
shopt -s globstar
mkdir temporary
sudo rsync -aHAXvh --include={audio,documents,informatics,pictures_videos,audio/**,documents/**,informatics/**,pictures_videos/**} --exclude "*" /mnt/$backup_drive_name/eray/* /home/eray

echo "umount drive"
sudo umount /mnt/$backup_drive_name
sudo rmdir /mnt/$backup_drive_name
sudo cryptsetup luksClose $backup_drive_name
