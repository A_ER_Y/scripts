#!/usr/bin/env bash
# create_iso.sh

lsblk -o name,fstype,size,type,mountpoint
read -p "select live usb: " LIVE_USB

echo "prepare iso"
rm -rf /home/eray/informatics/os/linux/archiso/*
cp -r /usr/share/archiso/configs/releng /home/eray/informatics/os/linux/archiso
echo "timeshift" >> /home/eray/informatics/os/linux/archiso/releng/packages.x86_64
cp /home/eray/informatics/os/linux/scripts/system/* /home/eray/informatics/os/linux/archiso/releng/airootfs/root
cp /home/eray/informatics/os/linux/scripts/timeshift_restore.sh /home/eray/informatics/os/linux/archiso/releng/airootfs/root

echo "build iso"
sudo mkarchiso -v -w /tmp/archiso -o /home/eray/informatics/os/linux/archiso /home/eray/informatics/os/linux/archiso/releng

if [[ "$LIVE_USB" != "" ]]; then
    echo "burn iso"
    sudo wipefs --all --force /dev/$LIVE_USB
    sudo pv /home/eray/informatics/os/linux/archiso/*.iso -Yo /dev/$LIVE_USB
fi
