#!/usr/bin/env bash
# 04_post_disk_preparation.sh

lsblk -o name,fstype,size,type,mountpoint
read -p "select boot partition: " BOOT_PARTITION

echo "create btrfs subvolumes"
btrfs subvolume create /mnt/@
mv /mnt/* /mnt/@ 2> /dev/null
btrfs subvolume create /mnt/@home
mv /mnt/@/home/* /mnt/@home
mount /dev/$BOOT_PARTITION /mnt/@/boot/efi

echo "configure fstab"
genfstab -U /mnt >> /mnt/@/etc/fstab
UUID=$(lsblk -f | grep cryptroot | tr -s " " | cut -d" " -f5)
sed -i "/$UUID/s/rw/defaults/" /mnt/@/etc/fstab
sed -i "/$UUID/s/subvolid=.,subvol=./subvol=@/" /mnt/@/etc/fstab
sed -i "/$UUID/s/.*/\0\nHOME_ENTRY_PLACEHOLDER/" /mnt/@/etc/fstab
FSTAB_HOME_ENTRY=$(grep $UUID /mnt/@/etc/fstab | sed -e "s_/_/home_" -e "s_@_@home_")
sed -i "s|HOME_ENTRY_PLACEHOLDER|$FSTAB_HOME_ENTRY|" /mnt/@/etc/fstab
sed -i "s_/@/boot/efi_/boot/efi_" /mnt/@/etc/fstab
