#!/usr/bin/env bash
# 06_secure_boot.sh

echo "enroll secure boot keys"
sbctl status
sudo sbctl create-keys
sudo sbctl enroll-keys -m

echo "sign .efi files"
sudo sbctl sign -so /usr/lib/systemd/boot/efi/systemd-bootx64.efi.signed /usr/lib/systemd/boot/efi/systemd-bootx64.efi
sudo sbctl sign -s /boot/efi/EFI/BOOT/BOOTX64.EFI
sudo sbctl sign -s /boot/efi/EFI/Linux/arch-linux.efi
sudo sbctl sign -s /boot/efi/EFI/Linux/arch-linux-fallback.efi
echo "reboot with 'systemctl reboot --firmware-setup' and enable secure boot"
