#!/usr/bin/env bash
# generate_ssl_certificate.sh

# https://github.com/amadoa/dotnet-devcert-linux

set -eu
ORG=localhost-ca
DOMAIN=localhost
path="$HOME/.ssl"

mkdir $HOME/.ssl 2> /dev/null
openssl genpkey -algorithm RSA -out "$path/ca.key"
openssl req -x509 -days 36500 -key "$path/ca.key" -out "$path/ca.crt" -subj "/CN=$ORG/O=$ORG"

openssl genpkey -algorithm RSA -out "$path/$DOMAIN".key
openssl req -new -key "$path/$DOMAIN".key -out "$path/$DOMAIN".csr -subj "/CN=$DOMAIN/O=$ORG"

openssl x509 -req -in "$path/$DOMAIN".csr -days 36500 -out "$path/$DOMAIN".crt \
    -CA "$path/ca.crt" -CAkey "$path/ca.key" -CAcreateserial \
    -extfile <(cat <<END
basicConstraints = CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
subjectAltName = DNS:$DOMAIN
END
    )

openssl pkcs12 -export -out "$path/$DOMAIN".pfx -inkey "$path/$DOMAIN".key -in "$path/$DOMAIN".crt
sudo trust anchor $path/ca.crt
