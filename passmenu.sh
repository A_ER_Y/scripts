#!/usr/bin/env bash
# passmenu.sh

# https://git.zx2c4.com/password-store/tree/contrib/dmenu/passmenu
# /usr/include/linux/input-event-codes.h

shopt -s nullglob globstar

prefix=${PASSWORD_STORE_DIR-~/.password-store}
password_files=( "$prefix"/**/*.gpg )
password_files=( "${password_files[@]#"$prefix"/}" )
password_files=( "${password_files[@]%.gpg}" )
password=$(printf '%s\n' "${password_files[@]}" | tofi "$@")

[[ -n $password ]] || exit
export GNUPGHOME=/home/eray/documents/.credentials/gpg-key
export PASSWORD_STORE_CLIP_TIME=5

if [[ ! $password =~ sudo|wifi ]]; then
    pass -c2 "$password" && wl-paste | { IFS= read -r pass; printf %s "$pass"; } | ydotool type --file -

    if [[ $password =~ google|gitlab|paypal ]]; then
        ydotool key 28:1 28:0
    elif [[ $password == "amazon" ]]; then
        ydotool key 15:1 15:0
        ydotool key 28:1 28:0
    elif [[ $password == "coolblue" ]]; then
        ydotool key 15:1 15:0
        ydotool key 15:1 15:0
    else
        ydotool key 15:1 15:0
    fi

    [[ $password =~ amazon|google|paypal ]] && sleep 3.5
fi

pass -c "$password" && wl-paste | { IFS= read -r pass; printf %s "$pass"; } | ydotool type --file - && ydotool key 28:1 28:0
