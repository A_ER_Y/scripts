#!/usr/bin/env bash
# download.sh

read -p "enter title: " TITLE
read -p "enter artist: " ARTIST

file=$TITLE
file=$(sed "s/[Ââ]/a/g" <<< $file)
file=$(sed "s/[Çç]/c/g" <<< $file)
file=$(sed "s/[Ğğ]/g/g" <<< $file)
file=$(sed "s/[İı]/i/g" <<< $file)
file=$(sed "s/[Öö]/o/g" <<< $file)
file=$(sed "s/[Şş]/s/g" <<< $file)
file=$(sed "s/[ÜüÛû]/u/g" <<< $file)
file=$(sed "s/ /_/g" <<< $file)
file=$(sed "s/\._/_/g" <<< $file)
file=$(sed "s/_-_/-/g" <<< $file)
file=$(sed "s/'//g" <<< $file)
file=$(tr "A-Z" "a-z" <<< $file)
file=$file.mp3

yt-dlp --no-playlist -x --audio-quality 0 --audio-format mp3 -o "$file" --exec after_video:"mid3v2 -D \"$file\"; mid3v2 -a \"$ARTIST\" -t \"$TITLE\" \"$file\"" $1
echo "Command to crop file: ffmpeg -i $file -ss 00:00:00 -to 00:00:00 -acodec copy cropped_file.mp3"
