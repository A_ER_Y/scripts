#!/usr/bin/env bash
# umount.sh

lsblk -o name,fstype,size,type,mountpoint
jmtpfs -l
read -p "select drive: " DRIVE_NAME

echo -e "\numount drive"
sudo umount /mnt/$DRIVE_NAME
sudo rmdir /mnt/$DRIVE_NAME

if [[ $DRIVE_NAME =~ _luks$ ]]; then
    sudo cryptsetup luksClose $DRIVE_NAME
fi
