#!/usr/bin/env bash
# web_search.sh

QUICKMARKS=/home/eray/informatics/os/linux/configuration_files/qutebrowser/quickmarks
input=$(cat $QUICKMARKS | cut -d" " -f1 | tofi)
[[ $input == "" ]] && exit

link=$(grep -E "$input.* " $QUICKMARKS 2> /dev/null | cut -d" " -f2 | head -n 1)
[[ $link == "" ]] &&
    setsid -f qutebrowser "$input" ||
    setsid -f qutebrowser "$link"
