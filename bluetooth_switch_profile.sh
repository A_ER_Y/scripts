#!/usr/bin/env bash
# bluetooth_switch_profile.sh

BLUEZ_CARD="bluez_card.88_C9_E8_F1_5D_F2"
BLUETOOTH_A2DP_PROFILE=$(pactl list cards | grep "Active Profile: a2dp-sink")
SINK=$(pactl get-default-sink)

pactl set-sink-volume $SINK 0
if [[ $BLUETOOTH_A2DP_PROFILE == "" ]]; then
    pactl set-card-profile $BLUEZ_CARD a2dp-sink
else
    pactl set-card-profile $BLUEZ_CARD headset-head-unit
fi
