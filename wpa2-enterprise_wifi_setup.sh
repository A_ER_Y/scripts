#!/usr/bin/env bash
# wpa2-enterprise_wifi_setup.sh

# https://github.com/wylermr/NetworkManager-WPA2-Enterprise-Setup

read -p "enter ssid: " SSID
read -p "enter login: " LOGIN
read -s -p "enter password: " PASSWORD
echo ""

cat <<EOF > /home/eray/temporary/"${SSID}"
[wifi-security]
key-mgmt=wpa-eap

[connection]
id=$SSID
uuid=$(uuidgen)
type=wifi

[ipv6]
method=auto

[wifi]
ssid=$SSID
mode=infrastructure
security=802-11-wireless-security

[802-1x]
eap=peap
identity=$LOGIN
phase2-auth=mschapv2
password=$PASSWORD

[ipv4]
method=auto
EOF

sudo chown root:root /home/eray/temporary/"${SSID}"
sudo chmod 600 /home/eray/temporary/"${SSID}"
sudo mv /home/eray/temporary/"${SSID}" /etc/NetworkManager/system-connections/
