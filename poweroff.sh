#!/usr/bin/env bash
# poweroff.sh

# https://bbs.archlinux.org/viewtopic.php?id=266965

lsblk -o name,fstype,size,type,mountpoint
read -p "select backup drive: " backup_drive_name

if [[ $backup_drive_name != "" ]]; then
    echo -e "\nmount backup drive"
    sudo cryptsetup luksOpen /dev/$backup_drive_name ${backup_drive_name}_luks
    backup_drive_name="${backup_drive_name}_luks"

    sudo mkdir -p /mnt/$backup_drive_name
    sudo mount /dev/mapper/$backup_drive_name /mnt/$backup_drive_name
fi


echo -e "\nmaintain snapshots"
SNAPSHOTS='sudo timeshift --list | tr -s " " | \grep -E "[0-9]{4}-[0-9]{2}-[0-9]{2}"'
SNAPSHOT_IDS=( $(eval "$SNAPSHOTS" | cut -d" " -f3) )
SNAPSHOT_DATES=( $(eval "$SNAPSHOTS" | cut -d" " -f3 | cut -d"_" -f1) )
SNAPSHOT_COMMENTS=( $(eval "$SNAPSHOTS" | cut -d" " -f5) )
WEEKLY_SNAPSHOTS="^day_[0-9]{2}$"
MONTHLY_SNAPSHOTS="^day_[0-9]{2}$|^week_[0-9]{2}$"
ANY_SNAPSHOT="^day_[0-9]{2}$|^week_[0-9]{2}$|^month_[0-9]{2}$|^year_[0-9]{4}$"
SNAPSHOTS_COUNT=$((${#SNAPSHOT_IDS[*]} - 1))
DAY=$(date +%d)
WEEK=$(date +%U)
MONTH=$(date +%m)
YEAR=$(date +%Y)
comment="day_$DAY"

for (( i = $(($SNAPSHOTS_COUNT)); i >= 0; i-- )); do
    delete=false

    if [[ ${SNAPSHOT_COMMENTS[$i]} =~ $ANY_SNAPSHOT ]] && [ ${SNAPSHOT_DATES[$i]} == "$YEAR-$MONTH-$DAY" ]; then
        comment="${SNAPSHOT_COMMENTS[$i]}"
        delete=true
    elif [[ ${SNAPSHOT_COMMENTS[$i]} =~ $WEEKLY_SNAPSHOTS ]] && [ $(date -d "${SNAPSHOT_DATES[$i]}" +%U) -ne $WEEK ] && [ $(date -d "${SNAPSHOT_DATES[$i]}" +%m) -eq $MONTH ] && [ $(date -d "tomorrow" +%m) -eq $MONTH ]; then
        comment="week_$WEEK"
        delete=true
    elif [[ ${SNAPSHOT_COMMENTS[$i]} =~ $MONTHLY_SNAPSHOTS ]] && [ $(date -d "${SNAPSHOT_DATES[$i]}" +%Y) -eq $YEAR ] && [ $(date -d "tomorrow" +%Y) -eq $YEAR ]; then
        if [ $(date -d "${SNAPSHOT_DATES[$i]}" +%m) -ne $MONTH ]; then
            comment="month_$(date -d '1 month ago' +%m)"
            delete=true
        elif [ $(date -d "tomorrow" +%m) -ne $MONTH ]; then
            comment="month_$MONTH"
            delete=true
        fi
    elif [[ ${SNAPSHOT_COMMENTS[$i]} =~ $ANY_SNAPSHOT ]]; then
        if [ $(date -d "${SNAPSHOT_DATES[$i]}" +%Y) -ne $YEAR ]; then
            comment="year_$(date -d '1 year ago' +%Y)"
            delete=true
        elif [ $(date -d "tomorrow" +%Y) -ne $YEAR ]; then
            comment="year_$YEAR"
            delete=true
        fi
    fi

    if $delete; then
        sudo timeshift --delete --snapshot "${SNAPSHOT_IDS[$i]}"
    else
        break
    fi
done


echo -e "\nclose tunnels"
killall ssh 2> /dev/null

echo "mute sound and mic"
SINK=$(pactl get-default-sink)
pactl set-sink-volume $SINK 0

SOURCE=$(pactl get-default-source)
pactl set-source-volume $SOURCE 0
pactl set-source-mute $SOURCE 1


echo "clear clipboard"
wl-copy --clear


echo "clear history"
rm /home/eray/.bash_history* 2> /dev/null
rm /home/eray/.lesshst 2> /dev/null
rm /home/eray/.python_history 2> /dev/null
rm /home/eray/.wget-hsts 2> /dev/null
rm /home/eray/.local/share/zathura/*history* 2> /dev/null

rm /home/eray/.config/qutebrowser/autoconfig.yml 2> /dev/null
find /home/eray/.local/share/qutebrowser/* -maxdepth 0 -type f ! -iname "adblock-cache.dat" -exec rm "{}" \;
rm -rf /home/eray/.local/share/qutebrowser/greasemonkey 2> /dev/null
rm -rf /home/eray/.local/share/qutebrowser/qutebrowser 2> /dev/null
rm -rf /home/eray/.local/share/qutebrowser/sessions 2> /dev/null
find /home/eray/.local/share/qutebrowser/webengine/* -maxdepth 0 -exec rm -rf "{}" \;

find /home/eray/.config/FreeTube/* -maxdepth 0 ! -iname "profiles.db" ! -iname "settings.db" -exec rm -rf "{}" \;


echo "clear unneeded files & directories"
find /home/eray/ -type d -name "__pycache__" -exec rm -rf "{}" \; 2> /dev/null
rm /home/eray/.local/state/nvim/shada/main.shada 2> /dev/null
rm /home/eray/.ssh/ssh_mux_* 2> /dev/null
rm /home/eray/debug.log 2> /dev/null
rm -rf /home/eray/.local/share/pnpm 2> /dev/null
rm -rf /home/eray/.local/state/pnpm 2> /dev/null
rm -rf /home/eray/.npm/_logs 2> /dev/null
sudo rm -rf /home/.pnpm-store 2> /dev/null
sudo rm -rf /root/.bash_history 2> /dev/null
sudo rm -rf /root/.cache 2> /dev/null
sudo rm -rf /root/.gnupg 2> /dev/null
sudo rm -rf /root/.local 2> /dev/null
sudo rm -rf /root/.ssh 2> /dev/null


echo "clear broken symbolic links"
find -L /home/eray -type l -exec rm -rf "{}" \; 2> /dev/null
sudo find -L /etc/ -type l -exec rm -rf "{}" \; 2> /dev/null


echo "clear docker"
clear_docker


echo -e "\nfreeup disk space"
bleachbit -c --preset 2> /tmp/bleachbit
sudo rm -rf $(grep -iE "permission denied" /tmp/bleachbit | cut -d":" -f2 | xargs)


echo "create snapshot"
sudo timeshift --create --comments "$comment"


if [[ $backup_drive_name != "" ]]; then
    echo -e "\nsync with backup drive"
    sudo rsync -aHAXvh --force --delete --delete-excluded --include={timeshift,timeshift/**,pictures_videos/[0-9]*,temporary} --exclude-from='/home/eray/informatics/os/linux/configuration_files/.rsyncignore' /home/* /mnt/$backup_drive_name

    echo -e "\numount drive"
    sudo umount /mnt/$backup_drive_name
    sudo rmdir /mnt/$backup_drive_name
    sudo cryptsetup luksClose $backup_drive_name
fi


echo -e "\npoweroff"
sleep 5
poweroff
