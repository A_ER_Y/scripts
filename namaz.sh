#!/usr/bin/env bash
# namaz.sh

if [ -z "$1" ]; then
    COUNTRY="belcika"
    CITY="zolder"
else
    while getopts ":C:c:" OPTIONS; do
        case ${OPTIONS} in
            c)          [ -z $COUNTRY ] && COUNTRY="belcika"; CITY=$OPTARG          ;;
            C)          COUNTRY=$OPTARG                                             ;;
            : | \?)     echo "usage: cmd [-c city] [-C country -c city]" && exit    ;;
        esac
    done
fi

curl -s https://www.namaztakvimi.com/$COUNTRY/$CITY-ezan-vakti.html | grep -Eo "[0-9]{2}:[0-9]{2} " > /tmp/.namaz.txt

if [ -s /tmp/.namaz.txt ]; then
    echo "$COUNTRY/$CITY"
    echo -e "current time: $(date +%R)\n"
    echo -e "tarih\timsak\tgüneş\tögle\tikindi\takşam\tyatsı"
    echo "-----------------------------------------------------"

    NUMBER_OF_DAYS=$(($(cat /tmp/.namaz.txt | wc -l) / 7 + 1))
    start_index=7
    end_index=12

    for (( i = 0; i < $NUMBER_OF_DAYS; i++ )); do
        sed -n "$start_index,${end_index}p" /tmp/.namaz.txt | echo -e "$(date -d "+$((i)) days" "+%d/%m")\t$(paste -s)"

        start_index=$(($start_index + 6))
        end_index=$(($end_index + 6))
    done
fi

rm /tmp/.namaz.txt
