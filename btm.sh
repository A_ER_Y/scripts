#!/usr/bin/env bash
# btm.sh

CONFIG_PATH=/home/eray/informatics/os/linux/configuration_files/bottom
IS_ON_VERTICAL_MONITOR=$(hyprctl layers | grep -A2 $(hyprctl activeworkspace -j | sed -e "s/[\", ]//g" | grep "monitor:" | cut -d":" -f2) | grep "1080 1920")
[[ "$IS_ON_VERTICAL_MONITOR" != "" ]] && LAYOUT_FILE=vertical_layout.toml || LAYOUT_FILE=horizontal_layout.toml

cat $CONFIG_PATH/bottom.toml $CONFIG_PATH/$LAYOUT_FILE > /home/eray/.config/bottom/bottom.toml
btm
