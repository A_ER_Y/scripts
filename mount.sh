#!/usr/bin/env bash
# mount.sh

lsblk -o name,fstype,size,type,mountpoint
jmtpfs -l
read -p "select drive: " drive_name

echo -e "\nmount drive"
if [[ ! $drive_name =~ ^[0-9]+$ ]]; then
    [[ ! -b /dev/$drive_name ]] && echo "drive does not exist" && exit

    DRIVE_FILESYSTEM_TYPE=$(lsblk -o name,fstype,size,type,mountpoint | grep "$drive_name" | tr -s " " | cut -d" " -f2)
    drive_path="/dev"

    if [ $DRIVE_FILESYSTEM_TYPE == "crypto_LUKS" ]; then
        sudo cryptsetup luksOpen $drive_path/$drive_name ${drive_name}_luks

        drive_name="${drive_name}_luks"
        drive_path="${drive_path}/mapper"
    fi
fi

sudo mkdir /mnt/$drive_name

if [[ ! $drive_name =~ ^[0-9]+$ ]]; then
    sudo mount $drive_path/$drive_name /mnt/$drive_name
else
    sudo chown -R eray:eray /mnt/$drive_name
    jmtpfs /mnt/$drive_name
fi
