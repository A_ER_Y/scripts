#!/usr/bin/env bash
# sync_phone.sh

jmtpfs -l
read -p "enter phone id: " PHONE_ID

echo -e "\nsync"
if [ -d /mnt/$PHONE_ID/Internal\ shared\ storage/eray ]; then
    rsync -urvh --delete --include={audio,audio/**} --exclude "*" /home/eray/* /mnt/$PHONE_ID/Internal\ shared\ storage/eray
    rsync -urvh --delete --include={documents,documents/assets,documents/assets/**} --exclude "*" /home/eray/* /mnt/$PHONE_ID/Internal\ shared\ storage/eray
    rsync -urvh --delete --include={documents,documents/diyanet,documents/diyanet/**} --exclude "*" /home/eray/* /mnt/$PHONE_ID/Internal\ shared\ storage/eray
    rsync -urvh --delete --include={documents,documents/id,documents/id/**} --exclude "*" /home/eray/* /mnt/$PHONE_ID/Internal\ shared\ storage/eray
    rsync -urvh --delete --include={documents,documents/islam,documents/islam/**} --exclude "*" /home/eray/* /mnt/$PHONE_ID/Internal\ shared\ storage/eray
    rsync -urvh --delete --include={documents,documents/military_service,documents/military_service/**} --exclude "*" /home/eray/* /mnt/$PHONE_ID/Internal\ shared\ storage/eray
    rsync -urvh --delete --include={documents,documents/other,documents/other/**} --exclude "*" /home/eray/* /mnt/$PHONE_ID/Internal\ shared\ storage/eray
    rsync -urvh --delete --include={documents,documents/special,documents/special/**} --exclude "*" /home/eray/* /mnt/$PHONE_ID/Internal\ shared\ storage/eray
    rsync -urvh --delete --include={documents,documents/travelling,documents/travelling/**} --exclude "*" /home/eray/* /mnt/$PHONE_ID/Internal\ shared\ storage/eray
    rsync -urvh --delete --include={documents,documents/wellness,documents/wellness/**} --exclude "*" /home/eray/* /mnt/$PHONE_ID/Internal\ shared\ storage/eray
    rsync -urvh --delete --include={pictures_videos,pictures_videos/[!0-9s]**} --exclude "*" /home/eray/* /mnt/$PHONE_ID/Internal\ shared\ storage/eray

    YEAR=2025
    rsync -aHAXvh --delete --include={pictures_videos,pictures_videos/$YEAR,pictures_videos/$YEAR/**} --exclude "*" /mnt/$PHONE_ID/Internal\ shared\ storage/eray/* /home/eray
    rsync -aHAXvh --delete --include={pictures_videos,pictures_videos/special,pictures_videos/special/$YEAR,pictures_videos/special/$YEAR/**} --exclude "*" /mnt/$PHONE_ID/Internal\ shared\ storage/eray/* /home/eray
    find /home/eray/pictures_videos/ -type d -exec sudo chmod g-w {} \;
else
    echo "/mnt/$PHONE_ID/Internal\ shared\ storage/eray does not exist"
fi
