#!/usr/bin/env bash
# bluetooth_connect.sh

# pair device
#   1. bluetoothctl
#   2. > scan on
#   3. press & hold button of both earbuds for ~5 seconds
#   4. > pair MAC_ADDRESS
#   5. > trust MAC_ADDRESS

MAC_ADDRESS="88:C9:E8:F1:5D:F2"
CONNECTED=$(bluetoothctl info $MAC_ADDRESS | grep "Connected: yes")
SINK=$(pactl get-default-sink)

pactl set-sink-volume $SINK 0
if [[ $CONNECTED == "" ]]; then
    bluetoothctl power on && bluetoothctl connect $MAC_ADDRESS
else
    bluetoothctl disconnect $MAC_ADDRESS && bluetoothctl power off
fi
