#!/usr/bin/env bash
# pinentry-tofi.sh

echo "OK Pleased to meet you"

while read stdin; do
    case $stdin in
        *BYE*) break ;;
        *SETDESC*) KEYNAME=${stdin#*:%0A%22}; KEYNAME=${KEYNAME%\%22\%0A*}; KEYID=${stdin#*ID }; KEYID=${KEYID%,*}; echo OK ;;
        *GETPIN*) echo -e "D $(echo "" | tofi --hide-input=true --prompt-text="Passphrase: ")\nOK" ;;
        *) echo OK
    esac
done
