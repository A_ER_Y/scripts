#!/usr/bin/env bash
# fzf.sh

EXCLUDED=".*\(/\.git\|.venv\|__pycache__\|/node_modules\|/vendor\).*"

if [[ "$1" != "d" ]]; then
    open $(find -type f -and -not -regex $EXCLUDED 2> /dev/null | sed "s|^\./||" | fzf --preview "cat {}")
else
    ls="eza -la --no-permissions --no-filesize --no-user --no-time --git --group-directories-first"
    cd $(find -type d -and -not -regex $EXCLUDED 2> /dev/null | sed "s|^\./||" | fzf --preview "$ls {}")
fi
