#!/usr/bin/env bash
# update.sh

echo "update mirrorlist"
sudo reflector --country Belgium,Netherlands,Germany --latest 3 --sort age --save /etc/pacman.d/mirrorlist

echo "update pacman packages"
sudo pacman -Syyu
sudo rm -rf /.boot 2> /dev/null
sudo cp -r /boot /.boot

echo -e "\nupdate aur packages"
paru -Sua --skipreview

echo -e "\nupdate nvim plugins"
nvim --headless -c "PackerSync" -c "TSUpdateSync" -c "MasonUpdate" -c "autocmd User MasonUpdateAllComplete quitall" -c "MasonUpdateAll"

echo -e "\nupdate adblock"
qutebrowser --nowindow :adblock-update & sleep 9 && killall qutebrowser
sleep 1

echo -e "\ncleanup"
sudo pacman -Rns $(pacman -Qdtq) 2> /dev/null
paru -cc
sudo paccache -rvuk0
sudo paccache -rvk3

echo -e "\nupdate man database"
sudo mandb
