# tunnels.sh

if [[ "$(pidof ssh)" == "" ]]; then
    /home/eray/informatics/os/linux/scripts/notify.sh "opening tunnels"

    PORT_PROXY_MAPPING=$(grep -E "^[^#]" /home/eray/informatics/work/jobs/inuits/port-proxy_mapping.txt | tr " " "|")
    for mapping in $PORT_PROXY_MAPPING; do
        port=$(cut -d "|" -f1 <<< $mapping)
        proxy=$(cut -d "|" -f2 <<< $mapping)
        ssh -F /home/eray/.ssh/inuits/config -D $port -f -N $proxy
    done

    /home/eray/informatics/os/linux/scripts/notify.sh "tunnels opened"
else
    killall ssh
    /home/eray/informatics/os/linux/scripts/notify.sh "tunnels closed"
fi
